<%--
  Created by IntelliJ IDEA.
  User: czmue
  Date: 3/4/2018
  Time: 2:11 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.pagen.model.Image" %>
<%@ page import="javax.print.StreamPrintService" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Login Success</title>
    <link href="/Pagen/assets/css/bootstrap-united.css" rel="stylesheet" />

</head>
<body>
<script src="/Pagen/jquery-1.8.3.js"></script>

<script src="/Pagen/bootstrap/js/bootstrap.js"></script>

<div class="navbar navbar-default">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                class="icon-bar"></span>
        </button>
    </div>

    <div class="navbar-collapse collapse navbar-responsive-collapse">
        <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search">
        </form>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/Pagen">Logout</a></li>
            <li class="dropdown"><a href="#" class="dropdown-toggle"
                                    data-toggle="dropdown">Explore<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Contact us</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Further Actions</a></li>
                </ul></li>
        </ul>
    </div>
</div>

<div align="center">
    <form action="/Pagen/ImageUpload" method="post" enctype="multipart/form-data">
        <label class="custom-file-label" for="customFile">Choose file</label>
        <input type="file" class="custom-file-input" id="customFile" name="img"><br/>
        <input class="btn btn-primary" type="submit">
        <input class="btn btn-default" type="reset">
    </form>
</div>
<iframe src="/Pagen/content/whiteboard.html" width=100% height=800 frameborder="0"></iframe>

<div align="center">
    <a class="btn btn-primary" href="/Pagen/content/login.jsp">Login as different user?</a>
</div>

<%
    String msg = (String)request.getAttribute("message");
    if(msg == "Upload Succeeded") {
        Image image = (Image) request.getAttribute("image");
        String htmlUrl = image.getHtmlUrl();
        request.setAttribute("htmlUrl", htmlUrl.substring(htmlUrl.lastIndexOf('/') + 1));
        RequestDispatcher rd = request.getRequestDispatcher("/download");
        rd.forward(request, response);
    }
%>

</body>
</html>
