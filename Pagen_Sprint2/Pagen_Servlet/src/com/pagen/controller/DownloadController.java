package com.pagen.controller;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }
    /**
     * GET handle html download
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String fullName = (String)request.getAttribute("htmlUrl");
        String fileName = fullName.substring(fullName.lastIndexOf('/') + 1);
        String htmlRepo = this.getServletContext().getRealPath("htmlInventory");
        response.setContentType(getServletContext().getMimeType(fileName)); // set MIME type
        response.setHeader("content-disposition", "attachment;filename=" + fileName); // set content-disposition
        File file = new File(htmlRepo + "/" + fileName);
        if(!file.exists()) {
            request.setAttribute("message", "The HTML file has been deleted.");
            request.getRequestDispatcher("success.jsp").forward(request, response);
        }
        FileInputStream in = new FileInputStream(file); // read file
        OutputStream out = response.getOutputStream(); //
        int len;
        while ((len = in.read()) != -1) {
            out.write(len);
        }
        in.close();
        out.close();
        if(file.exists() && file.isFile()) {
            if(file.delete()) System.out.println("HTML Deletion Succeeded");
            else System.out.println("HTML Deletion Failed");
        }
    }
}
