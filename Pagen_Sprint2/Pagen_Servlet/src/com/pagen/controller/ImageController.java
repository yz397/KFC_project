package com.pagen.controller;

import java.io.File;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.io.IOException;

import com.pagen.model.*;
import com.pagen.util.JavaSocket;

public class ImageController extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(System.getProperty("java.io.tmpdir"))); // location of temp file
        ServletFileUpload upload = new ServletFileUpload(factory);
        boolean multipartContent = ServletFileUpload.isMultipartContent(request); // is multipart
        String msg = "", host = "LAPTOP-ATMMV42V";
        int port = 12345;
        Image image = new Image();
        if (multipartContent) {
            try {
                List<FileItem> items = upload.parseRequest(request);
                for(FileItem item : items) {
                    if (!item.isFormField()) { // is File
                        String filePath = processUploadedFile(item);
                        String htmlPath = transferHandler(host, port, filePath);
                        image.setHtmlUrl(htmlPath);
                        image.setImageUrl(filePath);
                        msg = "Upload Succeeded";
                    }
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
                msg = "Upload Failed";
            } catch (Exception e) {
                e.printStackTrace();
                msg = "Save DB Failed";
            }

        } else {
            msg = "Invalid Format";
        }
        request.setAttribute("message", msg);
        request.setAttribute("image", image);
        request.getRequestDispatcher("content/success.jsp").forward(request, response);
    }

    /**
     * Process file upload
     */
    @SuppressWarnings("unused")
    private String processUploadedFile(FileItem item) {
        // get servlet context
        ServletContext sctx = this.getServletContext();
        // get images dir
        String imageUrl = sctx.getRealPath("imagesInventory");
        // get html files
        String htmlUrl = sctx.getRealPath("htmlInventory");
        // get filename
        String fileName = item.getName();
        // get file type
        String imageType = fileName.substring(fileName.lastIndexOf("."));
        // get system time
        String serialName = String.valueOf(System.currentTimeMillis());
        // create full file path name
        String pathname = imageUrl + "/" + fileName + serialName + imageType;
        File uploadFile = new File(pathname);
        // get file size
        long sizeInBytes = item.getSize();
        // store file
        try {
            item.write(uploadFile);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return pathname;
    }
    private String transferHandler(String host, int port, String filePath) throws IOException {
        JavaSocket socket = new JavaSocket(host, port);
        String htmlPath =  socket.File2HTML(filePath);
        File imgFile = new File(filePath);
        if(imgFile.exists() && imgFile.isFile()) {
            if(imgFile.delete()) System.out.println("Image Deletion Succeeded");
            else System.out.println("Image Deletion Failed");
        }
        return htmlPath;
    }
}
