package com.pagen.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class JavaSocket {
    private Socket socket;
    public JavaSocket(String host,int port) throws IOException{
        this.socket = new Socket(host,port);
        System.out.println("Connection Established");
    }
    public String File2HTML(String imgPath) throws IOException{
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        pw.write(imgPath);
        pw.flush();
        InputStream is = socket.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String HTMLPath ="";
        String temp="";
        while((temp=br.readLine())!=null){
            HTMLPath += temp;
        }
        br.close();
        is.close();
        pw.close();
        os.close();
        return HTMLPath;
    }
}